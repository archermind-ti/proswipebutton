# ProSwipeButton

### 简介

适用于OHOS的滑动按钮，带有用于异步操作的圆形进度栏

### 功能

1. 可滑动的圆形进度按钮
2. 可切换状态、重置状态
3. 丰富的可配置项（按钮背景、按钮文字颜色、按钮文字大小、滑块颜色和按钮圆角大小）

### 演示效果

![proSwipeButton_demo.gif](proSwipeButton_demo.gif)

### 集成说明

在module的build.gradle中添加对`ProSwipeButton`的依赖

```groovy
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	implementation 'com.gitee.archermind-ti:proswipebutton_ohos:1.0.0'
    ……
}
```

在project的build.gradle中添加`mavenCentral()`的引用

``` groovy
allprojects {
    repositories {
        ……
        mavenCentral()
    }
}
```



### 使用说明

1. 在您的XML布局文件中，添加此自定义视图

```xml
<in.shadowfax.proswipebutton.ProSwipeButton
        ohos:id="$+id:proswipebutton_main_error"
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:margin="8vp"
        app:bg_color="#0099cc"
        app:btn_text="Place Order"
        />
```

2. 通过添加滑动监听器来对按钮上的成功滑动做出反应

```java
final ProSwipeButton proSwipeBtnError = (ProSwipeButton) findComponentById(ResourceTable.Id_proswipebutton_main_error);
proSwipeBtn.setSwipeDistance(0.85f);

proSwipeBtn.setOnSwipeListener(
    () -> getUITaskDispatcher().delayDispatch(
        () -> proSwipeBtn.showResultIcon(true, false), 2000)
);
```

3. 异步任务完成后，告诉ProSwipeButton显示结果图标。 滴答声表示成功的异步操作，或者划叉表示失败的异步操作。

```java
proSwipeBtn.showResultIcon(true); //if task succeeds
proSwipeBtn.showResultIcon(false); //if task fails
```

设置用户必须滑动指定距离后才能激活按钮。

```java
proSwipeBtn.setSwipeDistance(0.85f);
```



#### 自定义属性

你可以通过XML或代码中定义的方式自定义按钮

```xml
<in.shadowfax.proswipebutton.ProSwipeButton
        ohos:id="$+id:proswipebutton"
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:margin="8vp"
        app:arrow_color="#33FFFFFF"
        app:bg_color="#0099cc"
        app:btn_radius="2vp"
        app:btn_text="Place Order"
        app:text_color="white"
        app:text_size="12fp"
        />
```



| xml属性     | 描述                                           |
| ----------- | ---------------------------------------------- |
| arrow_color | 表示滑块（箭头）颜色                           |
| bg_color    | 表示按钮背景颜色，同时也是圆形进度条的背景颜色 |
| btn_radius  | 表示按钮背景的圆角大小                         |
| btn_text    | 表示按钮的文本                                 |
| text_color  | 表示按钮的文本颜色                             |
| text_size   | 表示按钮的文本大小                             |

### 编译说明

1. 将项目通过git clone 至本地
2. 使用DevEco Studio 打开该项目，然后等待Gradle 构建完成
3. 点击`Run`运行即可（真机运行可能需要配置签名）

### 版权和许可信息

```MIT License
MIT License

Copyright (c) 2017 Shadowfax Technologies

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

