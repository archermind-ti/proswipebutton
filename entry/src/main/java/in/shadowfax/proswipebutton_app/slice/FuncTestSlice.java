/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package in.shadowfax.proswipebutton_app.slice;

import in.shadowfax.proswipebutton.ProSwipeButton;
import in.shadowfax.proswipebutton_app.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.AttrHelper;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;

public class FuncTestSlice extends AbilitySlice {

    private int state = 0;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_slice_func_test);

        ProSwipeButton proswipebutton = (ProSwipeButton) findComponentById(ResourceTable.Id_proswipebutton);

        final float textSize = proswipebutton.getTextSize();
        final int textColor = proswipebutton.getTextColor();
        final String text = proswipebutton.getText();
        final int bgColor = proswipebutton.getBackgroundColor();
        final float cornerRadius = proswipebutton.getCornerRadius();
        final int arrowColor = proswipebutton.getArrowColorRes();
        final float distance = proswipebutton.getSwipeDistance();

        proswipebutton.setOnSwipeListener(() -> getUITaskDispatcher().delayDispatch(() -> {
            switch (state) {
                case 1:
                    proswipebutton.showResultIcon(true);
                    break;
                case 2:
                    proswipebutton.showResultIcon(false);
                    break;
                case 0:
                case 3:
                    proswipebutton.showResultIcon(true, true);
                    break;
                case 4:
                    proswipebutton.showResultIcon(true, false);
                    break;
                case 5:
                    proswipebutton.showResultIcon(false, true);
                    break;
                case 6:
                    proswipebutton.showResultIcon(false, false);
                    break;
            }
        }, 2000));

        findComponentById(ResourceTable.Id_btn_show_success).setClickedListener(v -> {
            state = 1;
            proswipebutton.performOnSwipe();
        });
        findComponentById(ResourceTable.Id_btn_show_fail).setClickedListener(v -> {
            state = 2;
            proswipebutton.performOnSwipe();
        });

        findComponentById(ResourceTable.Id_btn_show_success_reset).setClickedListener(v -> {
            state = 3;
            proswipebutton.performOnSwipe();
        });
        findComponentById(ResourceTable.Id_btn_show_success_noreset).setClickedListener(v -> {
            state = 4;
            proswipebutton.performOnSwipe();
        });

        findComponentById(ResourceTable.Id_btn_show_fail_reset).setClickedListener(v -> {
            state = 5;
            proswipebutton.performOnSwipe();
        });
        findComponentById(ResourceTable.Id_btn_show_fail_noreset).setClickedListener(v -> {
            state = 6;
            proswipebutton.performOnSwipe();
        });

        findComponentById(ResourceTable.Id_btn_set_text).setClickedListener(v -> proswipebutton.setText("Custom Text"));
        findComponentById(ResourceTable.Id_btn_get_text).setClickedListener(v -> toast(proswipebutton.getText()));

        findComponentById(ResourceTable.Id_btn_set_text_color).setClickedListener(v -> proswipebutton.setTextColor(Color.BLUE.getValue()));
        findComponentById(ResourceTable.Id_btn_get_text_color).setClickedListener(v -> toast(String.valueOf(proswipebutton.getTextColor() == Color.BLUE.getValue())));

        findComponentById(ResourceTable.Id_btn_set_bg_color).setClickedListener(v -> proswipebutton.setBackgroundColor(Color.BLUE.getValue()));
        findComponentById(ResourceTable.Id_btn_get_bg_color).setClickedListener(v -> toast(String.valueOf(proswipebutton.getBackgroundColor() == Color.BLUE.getValue())));

        findComponentById(ResourceTable.Id_btn_set_corner_radius).setClickedListener(v -> proswipebutton.setCornerRadius(AttrHelper.vp2px(10, getContext())));
        findComponentById(ResourceTable.Id_btn_get_corner_radius).setClickedListener(v -> toast(String.valueOf(proswipebutton.getCornerRadius() == AttrHelper.vp2px(10, getContext()))));

        findComponentById(ResourceTable.Id_btn_set_arrow_color).setClickedListener(v -> proswipebutton.setArrowColor(Color.WHITE.getValue()));
        findComponentById(ResourceTable.Id_btn_get_arrow_color).setClickedListener(v -> toast(String.valueOf(proswipebutton.getArrowColorRes() == Color.WHITE.getValue())));

        findComponentById(ResourceTable.Id_btn_set_text_size).setClickedListener(v -> proswipebutton.setTextSize(AttrHelper.fp2px(16, getContext())));
        findComponentById(ResourceTable.Id_btn_get_text_size).setClickedListener(v -> toast(String.valueOf(proswipebutton.getTextSize() == AttrHelper.fp2px(16, getContext()))));

        findComponentById(ResourceTable.Id_btn_set_swipe_distance).setClickedListener(v -> proswipebutton.setSwipeDistance(0.5f));
        findComponentById(ResourceTable.Id_btn_get_swipe_distance).setClickedListener(v -> toast(String.valueOf(proswipebutton.getSwipeDistance() == 0.5f)));

        findComponentById(ResourceTable.Id_btn_perform).setClickedListener(v -> proswipebutton.performOnSwipe());
        findComponentById(ResourceTable.Id_btn_reset).setClickedListener(v -> {
            proswipebutton.setTextSize((int) textSize);
            proswipebutton.setTextColor(textColor);
            proswipebutton.setText(text);
            proswipebutton.setBackgroundColor(bgColor);
            proswipebutton.setArrowColor(arrowColor);
            proswipebutton.setCornerRadius(cornerRadius);
            proswipebutton.setSwipeDistance(distance);
            proswipebutton.resetStatus();
        });
    }

    private void toast(String text) {
        new ToastDialog(this).setText(text).show();
    }
}
