package in.shadowfax.proswipebutton_app.slice;

import in.shadowfax.proswipebutton.ProSwipeButton;
import in.shadowfax.proswipebutton_app.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        final ProSwipeButton proSwipeBtn = (ProSwipeButton) findComponentById(ResourceTable.Id_proswipebutton_main);
        final ProSwipeButton proSwipeBtnError = (ProSwipeButton) findComponentById(ResourceTable.Id_proswipebutton_main_error);
        proSwipeBtn.setSwipeDistance(0.85f);

        proSwipeBtn.setOnSwipeListener(() -> getUITaskDispatcher().delayDispatch(() -> proSwipeBtn.showResultIcon(true, false), 2000));

        proSwipeBtnError.setOnSwipeListener(() -> {
            // user has swiped the btn. Perform your async operation now
            getUITaskDispatcher().delayDispatch(() -> proSwipeBtnError.showResultIcon(false, true), 2000);
        });

        findComponentById(ResourceTable.Id_btn_test).setClickedListener(v -> present(new FuncTestSlice(), new Intent()));
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
