package in.shadowfax.proswipebutton;

import ohos.agp.components.AttrHelper;
import ohos.app.Context;

public class Constants {
    public static final float DEFAULT_SWIPE_DISTANCE = 0.85f;
    public static final int MORPH_ANIM_DURATION = 500;

    public static float getDEFAULT_TEXT_SIZE(Context context) {
        return AttrHelper.vp2px(14, context);
    }

    public static float getBTN_INIT_RADIUS(Context context) {
        return AttrHelper.vp2px(2, context);
    }

    public static float getBTN_MORPHED_RADIUS(Context context) {
        return AttrHelper.vp2px(100, context);
    }

    public static int getProgressBorder(Context context) {
        return AttrHelper.vp2px(5, context);
    }
}
